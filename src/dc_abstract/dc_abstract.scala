package dc_abstract

import scala.util.Random

trait T {
  def norm(ctx: NT): NT
}
case class ClassT(name: String, parameters: Map[String, T] = Map()) extends T {
  def norm(ctx: NT): NT = {
    ClassNT(name, parameters.map { case (f, t) => f -> t.norm(ctx) })
  }
}
trait DepT extends T

trait P extends DepT
case class ThisP() extends P {
  def norm(ctx: NT): NT = ctx
}
case class PLookup(path: P, field: String) extends P {
  def norm(ctx: NT): NT = path.norm(ctx) match {
    case ClassNT(_, parameters) => parameters(field).norm(ctx)
    case int @ InitNT(_, parameters) => {
      println("PLookup.parameters: " + parameters.mkString(", "));
      println(s"PLookup.ctx: $ctx")
      println(s"PLookup.int: $int")
      parameters(field).norm(ctx)
    }
  }
}

trait Expr {
  def typecheck(ctx: NT, classes: List[Class]): NT
  def reduce(classes: List[Class]): Val
  def substThis(ctx: Expr): Expr
}

// not sure what to do about EmptyBody...
case class EmptyBody() extends Expr {
  def typecheck(ctx: NT, classes: List[Class]) = ???
  def reduce(classes: List[Class]): Val = ???
  def substThis(ctx: Expr): Expr = ???
}

trait Val extends Expr with DepT with NT {
  override def isSubtypeOf(tpe: NT, classes: List[Class], ctx: NT) = tpe == this
}

case class Obj(name: String, parameters: Map[String, Val] = Map()) extends Val {
  def norm(ctx: NT): NT = this
  def typecheck(ctx: NT, classes: List[Class]) = this

  override def isSubtypeOf(tpe: NT, classes: List[Class], ctx: NT) = {
    ClassNT(name, parameters).isSubtypeOf(tpe, classes, ctx)
  }

  def reduce(classes: List[Class]): Val = this

  def substThis(ctx: Expr): Expr = {
    Obj(name, parameters.map { case (f, e) => (f, e.substThis(ctx).asInstanceOf[Val]) })
  }
}
case class FLookup(expr: Expr, field: String) extends Expr {
  def typecheck(ctx: NT, classes: List[Class]) = {
    val tpdExpr = expr.typecheck(ctx, classes).norm(ctx)
    tpdExpr match {
      case ClassNT(_, parameters) => parameters(field)
      case InitNT(_, parameters) => parameters(field)
      case Obj(_, parameters) => parameters(field)
      case _ => throw new TypeException(s"$expr is not of type ClassNT or Obj");
    }
    //RefNT(tpdExpr, field, ctx)
  }

  def reduce(classes: List[Class]): Val = {
    expr.reduce(classes) match { case Obj(_, parameters) => parameters(field).reduce(classes) }
  }

  def substThis(ctx: Expr): Expr = FLookup(expr.substThis(ctx), field)
}
case class This() extends Expr {
  def typecheck(ctx: NT, classes: List[Class]) = ctx

  def reduce(classes: List[Class]): Val = ???

  def substThis(ctx: Expr): Expr = ctx
}

object Init {
  var uid_counter = 0
  
  def uniqueID = {uid_counter+=1; uid_counter}
}

case class Init(name: String, parameters: Map[String, Expr] = Map()) extends Expr {
  var uid: Int = 0//Init.uniqueID
  
  def typecheck(ctx: NT, classes: List[Class]) = {
    val newCtx = ctx //if (ctx.isInstanceOf[InitNT]) ctx else this.getCtx(classes)

    val tpedArguments = parameters.map { case (f, e) => (f, e.typecheck(newCtx, classes)) }

    val matchingArguments = matchingClasses(classes).filter { c =>
      println(s"c.parameters: ${c.parameters}")
      val tpedClassArguments = c.parameters.map { case (f, t) => (f, t.norm(ctx)) }
      println(s"Init inside: ${c.name}: $tpedClassArguments")
      tpedArguments.forall { case (f, nt) => nt.isSubtypeOf(tpedClassArguments(f), classes, newCtx) }
    }
    if (matchingArguments.isEmpty)
      throw new TypeException(s"Did not find any class that matches the arguments of ${tpedArguments}")

    val initNT = InitNT(name, tpedArguments)
    initNT.uid = uid
    initNT
  }

  def reduce(classes: List[Class]): Val = {
    val newCtx = this.getCtx(classes)
    
    val tpedArguments = parameters.map { case (f, e) => (f, e.typecheck(newCtx, classes)) }

    val cls = matchingClasses(classes).filter { c =>
      println(s"c.parameters: ${c.parameters}")
      val tpedClassArguments = c.parameters.map { case (f, t) => (f, t.norm(newCtx)) }
      println(s"Init inside: ${c.name}: $tpedClassArguments")
      tpedArguments.forall { case (f, nt) => nt.isSubtypeOf(tpedClassArguments(f), classes, newCtx) }
    }.head
    cls.body.substThis(Obj(name, parameters.map { case (f, e) => (f, e.reduce(classes)) })).asInstanceOf[Val]
  }

  def substThis(ctx: Expr): Expr = {
    Init(name, parameters.map { case (f, e) => (f, e.substThis(ctx)) })
  }

  def matchingClasses(classes: List[Class]) = {
    classes.filter { c =>
      c.name == name &&
        c.parameters.keys.forall { s => parameters.keys.exists { s2 => s2 == s } } &&
        parameters.keys.forall { s => c.parameters.keys.exists { s2 => s2 == s } }
    }
  }

  def normFix(params: Map[String, NT], classes: List[Class]): Map[String, NT] = {
    println(s"$this.normFix($params, $classes)")
    if (parameters.isEmpty)
      params
    else {
      var res: Map[String, NT] = Map()
      parameters.foreach {
        case (n, t) =>
          try {
            val newCtx = InitNT(name, params)
            newCtx.uid = uid
            println(s"newCtx: $newCtx")
            val tpedParam = t.typecheck(newCtx, classes)
            println(s"normFix: ($n, $t): $params == $tpedParam")

            val normRes = Init(name, parameters - n).normFix(params + (n -> tpedParam), classes)
            if (res.isEmpty)
              res = normRes
          } catch { case _: Exception => }
      }
      println(s"$this.normFix($params, $classes) == $res")
      res
    }
  }

  def getCtx(classes: List[Class]): InitNT = { val res = InitNT(this.name, this.normFix(Map(), classes)); res.uid = uid; println(s"$this.getCtx() == $res"); res }
}

case class Class(name: String, parameters: Map[String, T] = Map(), ext: List[ClassT] = List(), body: Expr = This(), ret: T = ThisP()) {
  def check(classes: List[Class]) {
    val ctx = this.getCtx
    println(ctx)

    val normedParameters = parameters.map { case (f, t) => (f -> t.norm(ctx)) }
    println("normedParams: " + normedParameters)

    println("body: " + body)

    val normedBody = body.typecheck(ctx, classes)
    println("normedBody: " + normedBody)

    val normedRet = ret.norm(ctx)

    println("normedRet: " + normedRet)

    if (!normedBody.isSubtypeOf(normedRet, classes, ctx))
      throw new TypeException(s"the type of the body ($normedBody) is not a subtype of the return type ($normedRet)")

    ext.foreach { ct =>
      ct.norm(ctx) match {
        case ClassNT(_, extParameters) =>
          if (!matchingClasses(classes).exists { c =>
            extParameters.forall { case (f, nt) => nt.isSubtypeOf(c.parameters(f).norm(c.getCtx), classes, ctx) }
          })
            throw new TypeException(s"extended class $ct is cannot be found in the classes list.")
      }
    }
  }

  def normFix(params: Map[String, NT]): Map[String, NT] = {
    if (parameters.isEmpty)
      params
    else {
      var res: Map[String, NT] = Map()
      parameters.foreach {
        case (n, t) =>
          try {
            val newCls = ClassNT(name, params)
            val tpedParam = t.norm(newCls)

            val normRes = Class(name, parameters - n, ext, body, ret).normFix(params + (n -> tpedParam))
            if (res.isEmpty)
              res = normRes
          } catch { case _: Exception => }
      }
      res
    }
  }

  def getCtx: ClassNT = ClassNT(this.name, this.normFix(Map()))

  def matchingClasses(classes: List[Class]) = {
    classes.filter { c => c.name == name }.filter { c =>
      c.parameters.keys.forall { s => parameters.keys.exists { s2 => s2 == s } } &&
        parameters.keys.forall { s => c.parameters.keys.exists { s2 => s2 == s } }
    }
  }
}

// normalized types
trait NT extends T {
  def isSubtypeOf(tpe: NT, classes: List[Class], ctx: NT): Boolean
}

trait ObjNT extends NT {

}

case class ClassNT(name: String, parameters: Map[String, NT]) extends NT {
  def norm(ctx: NT): NT = this
  def isSubtypeOf(tpe: NT, classes: List[Class], ctx: NT): Boolean = {
    val res =
      (tpe == this) || (tpe match {
        case ClassNT(tpeName, tpeParameters) => matchingClasses(classes).filter { c =>
          (tpeParameters.forall { case (f, nt) => (c.parameters.contains(f) && parameters(f).isSubtypeOf(nt, classes, ctx)) } &&
            c.parameters.forall { case (f, _) => tpeParameters.exists { case (ft, _) => ft == f } }) ||
            c.ext.exists { ct => ct.norm(ctx).isSubtypeOf(tpe, classes, ctx) }
        }.size > 0

        case _ => false
      })
    println(s"$this.isSubtypeOf($tpe) == $res")
    res
  }

  def matchingClasses(classes: List[Class]) = {
    classes.filter { c => c.name == name }.filter { c =>
      c.parameters.keys.forall { s => parameters.keys.exists { s2 => s2 == s } } &&
        parameters.keys.forall { s => c.parameters.keys.exists { s2 => s2 == s } }
    }
  }
}

case class InitNT(name: String, parameters: Map[String, NT]) extends ObjNT {
  var uid: Integer = 0
  
  def norm(ctx: NT): NT = this
  def isSubtypeOf(tpe: NT, classes: List[Class], ctx: NT): Boolean = {
    val res = tpe match {
      case int @ InitNT(iname, iparameters) => iname == name && uid == int.uid && parameters.forall { case (f, t) => iparameters.contains(f) && t.isSubtypeOf(iparameters(f), classes, ctx) }
      case cnt @ ClassNT(tpeName, tpeParameters) => matchingClasses(classes).filter { c =>
        // TODO: check if classes correlate
        tpeParameters.forall { case (f, nt) => (c.parameters.contains(f) && parameters(f).isSubtypeOf(nt, classes, ctx)) }
      }.size > 0
      case _ => false
    }
    println(s"$this.isSubtypeOf($tpe) == $res")
    res
  }

  def matchingClasses(classes: List[Class]) = {
    classes.filter { c => c.name == name }.filter { c =>
      c.parameters.keys.forall { s => parameters.keys.exists { s2 => s2 == s } } &&
        parameters.keys.forall { s => c.parameters.keys.exists { s2 => s2 == s } }
    }
  }
}

case class RefNT(t: NT, field: String, ctx: NT) extends ObjNT {
  def norm(ctx: NT): NT = this
  def isSubtypeOf(tpe: NT, classes: List[Class], sctx: NT): Boolean = {
    val res = tpe match {
      case RefNT(rt, rfield, rctx) => rfield == field && t.isSubtypeOf(rt, classes, sctx) && rctx == ctx
      case _ => false
    }
    println(s"RefNT ctx: $ctx")
    println(s"$this.isSubtypeOf($tpe) == $res")
    res
  }
}