package dc_abstract

/**
 * @author manuel
 */
class TypeException(message: String = null, cause: Throwable = null) extends RuntimeException(message, cause) {
  
}