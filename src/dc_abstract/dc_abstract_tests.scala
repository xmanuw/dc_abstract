package dc_abstract

import org.scalatest._

/**
 * @author manuel
 */

class dc_abstract_tests extends FlatSpec with Matchers {
  val cls = Class("Test", Map("test" -> ThisP()), List(), EmptyBody(), ThisP())
  println(cls)

  val expr = Init("Test", Map("test" -> This()))
  println(expr)

  val tNat = Class("Nat", Map(), List(), This(), ThisP())

  val tZero = Class("Zero", Map(), List(ClassT("Nat", Map())), This(), ThisP())
  "Zero" should "check" in {
    tZero.check(List(tZero, tNat))
  }

  val tNewZero = Init("Zero", Map())
  it should "typecheck correctly" in {
    val classes = List(tZero, tNat)
    tNewZero.typecheck(tNewZero.getCtx(classes), classes) should equal(InitNT("Zero", Map()))
  }
  it should "reduce correctly" in {
    tNewZero.reduce(List(tZero, tNat)) should equal(Obj("Zero", Map()))
  }

  val tSomeNat = Class("SomeNat", Map("zero2" -> PLookup(ThisP(), "zero"), "zero" -> ClassT("Zero", Map())), List(ClassT("Nat", Map())), This(), ThisP())
  "SomeNat" should "check" in {
    tSomeNat.check(List(tSomeNat, tZero, tNat))
  }

  val tSucc = Class("Succ", Map("pred" -> ClassT("Nat", Map())), List(ClassT("Nat", Map())), This(), ThisP())
  "Succ" should "check" in {
    tSucc.check(List(tZero, tNat, tSucc))
  }

  val tOne = Init("Succ", Map("pred" -> Init("Zero", Map())))
  "One" should "typecheck correctly" in {
    val classes = List(tZero, tNat, tSucc)
    tOne.typecheck(tOne.getCtx(List(tZero, tNat, tSucc)), classes) should equal(InitNT("Succ", Map("pred" -> InitNT("Zero", Map()))))
  }
  
  it should "reduce correctly" in {
    tOne.reduce(List(tZero, tNat, tSucc)) should equal (Obj("Succ", Map("pred" -> Obj("Zero"))))
  }
  
  val tSucc2 = Class("Succ2", Map("pred" -> ClassT("Zero", Map())), List(ClassT("Succ", Map("pred" -> PLookup(ThisP(), "pred")))))
  "Succ2(pred: Zero) extends Succ(pred)" should "check" in {
    tSucc2.check(List(tZero, tNat, tSucc, tSucc2))
  }
  
  val tNewSucc2 = Init("Succ2", Map("pred" -> Init("Zero")))
  "new Succ(pred = new Zero)" should "typecheck" in {
    val classes = List(tZero, tNat, tSucc2, tSucc)
    tNewSucc2.typecheck(tNewSucc2.getCtx(classes), classes)
  }
  

  val tAdd = Class("Add", Map("n1" -> ClassT("Nat", Map()), "n2" -> ClassT("Nat", Map())), List(), Init("Zero", Map()), ClassT("Nat", Map()))
  "Add(n1: Nat, n2: Nat)" should "check" in {
    tAdd.check(List(tZero, tNat, tSucc))
  }

  val tAddZero = Class("Add", Map("n1" -> ClassT("Zero", Map()), "n2" -> ClassT("Nat", Map())), List(), Init("Zero", Map()), PLookup(ThisP(), "n2"))
  "Add(n1: Zero, n2: Nat)" should "check" in {
    tAddZero.check(List(tZero, tNat, tAdd, tSucc, tAddZero))
  }

  val tAddSucc = Class("Add",
    Map("n1" -> ClassT("Succ", Map("pred" -> ClassT("Nat", Map()))), "n2" -> ClassT("Nat", Map())),
    List(),
    Init("Succ", Map("pred" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "n1"), "pred"), "n2" -> FLookup(This(), "n2"))))),
    ClassT("Nat", Map()))
  "Add(n1: Succ(pred: Nat), n2: Nat)" should "check" in {
    tAddSucc.check(List(tZero, tNat, tAdd, tSucc, tAddSucc, tAddZero))
  }

  def makeNat(n: Int): Init = {
    if (n == 0)
      tNewZero
    else
      Init("Succ", Map("pred" -> makeNat(n - 1)))
  }
  
  def makeNatVal(n: Int): Obj = {
    if (n == 0)
      Obj("Zero")
    else
      Obj("Succ", Map("pred" -> makeNatVal(n - 1)))
  }
  
  def makeNatAdd(n1: Int, n2: Int): Init = {
    Init("Add", Map("n1" -> makeNat(n1), "n2" -> makeNat(n2)))
  }
  
  val natClasses = List(tNat, tZero, tSucc, tAddZero, tAddSucc)
  
  "0 + 0" should "reduce to 0" in {
    makeNatAdd(0, 0).reduce(natClasses) should equal (makeNatVal(0))
  }
  
  "test" should "test" in {
    val twoplus3 = makeNatAdd(2, 3)
    val tpe = twoplus3.typecheck(twoplus3.getCtx(natClasses), natClasses)
    println("twoplus3 tpe:" + tpe)
    tpe.isSubtypeOf(ClassNT("Nat", Map()), natClasses, twoplus3.getCtx(natClasses))
  }

  def tSpace = Class("Space", Map(), List(), This(), ThisP())
  "Space" should "check" in {
    tSpace.check(List(tSpace))
  }

  def t2DSpace = Class("2DSpace", Map(), List(ClassT("Space")))
  "2DSpace" should "check" in {
    t2DSpace.check(List(tSpace, t2DSpace))
  }

  def t3DSpace = Class("3DSpace", Map(), List(ClassT("Space")))
  "3DSpace" should "check" in {
    t3DSpace.check(List(tSpace, t3DSpace))
  }

  def tPoint = Class("Point", Map("s" -> ClassT("Space")))
  "Point(s: Space)" should "check" in {
    tPoint.check(List(tPoint, tSpace))
  }

  def t2DPoint = Class("Point", Map("s" -> ClassT("2DSpace"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat")), List(ClassT("Point", Map("s" -> PLookup(ThisP(), "s")))))
  "Point(s: 2DSpace, x: Nat, y: Nat) extends Point(s)" should "check" in {
    t2DPoint.check(List(tPoint, t2DPoint, tSpace, t2DSpace))
  }

  it should "be a subtype of Point(s: Space)" in {
    t2DPoint.getCtx.isSubtypeOf(tPoint.getCtx, List(tPoint, t2DPoint, tSpace, t2DSpace), t2DPoint.getCtx) should equal(true)
  }

  def tNew2DPoint = Init("Point", Map("s" -> Init("2DSpace"), "x" -> Init("Zero"), "y" -> Init("Zero")))
  it should "init correctly" in {
    val classes = List(tPoint, t2DPoint, tSpace, t2DSpace, tZero)
    tNew2DPoint.typecheck(tNew2DPoint.getCtx(classes), classes) should equal(InitNT("Point", Map("s" -> InitNT("2DSpace", Map()), "x" -> InitNT("Zero", Map()), "y" -> InitNT("Zero", Map()))))
  }

  def t2DPointWrongExt = Class("Point", Map("s" -> ClassT("2DSpace"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat")))
  "Point(s: 2DSpace, x: Nat, y: Nat)" should "not be a subtype of Point(s: Space)" in {
    t2DPointWrongExt.getCtx.isSubtypeOf(tPoint.getCtx, List(tPoint, t2DPointWrongExt, tSpace, t2DSpace), t2DPointWrongExt.getCtx) should equal(false)
  }

  def tAdd2D = Class("Add", Map("s" -> ClassT("2DSpace"),
    "p1" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"))),
    "p2" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat")))),
    List(),
    Init("Point", Map("s" -> FLookup(This(), "s"),
      "x" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "p1"), "x"), "n2" -> FLookup(FLookup(This(), "p2"), "x"))),
      "y" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "p1"), "y"), "n2" -> FLookup(FLookup(This(), "p2"), "y"))))),
    ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"))))

  "Add(s: 2DSpace, p1: Point(s: this.s, x: Nat, y: Nat), p2: Point(s: this.s, x: Nat, y: Nat))" should "check" in {
    tAdd2D.check(List(tAdd, tAdd2D, tZero, t2DSpace, tSpace, tNat, t2DPoint))
  }

  def t3DPoint = Class("Point", Map("s" -> ClassT("3DSpace"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"), "z" -> ClassT("Nat")), List(ClassT("Point", Map("s" -> PLookup(ThisP(), "s")))))
  "Point(s: 3DSpace, x: Nat, y: Nat, z: Nat)" should "check" in {
    t3DPoint.check(List(tPoint, t3DPoint, tSpace, t3DSpace))
  }

  def tAdd3D = Class("Add", Map("s" -> ClassT("3DSpace"),
    "p1" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"), "z" -> ClassT("Nat"))),
    "p2" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"), "z" -> ClassT("Nat")))),
    List(),
    Init("Point", Map("s" -> FLookup(This(), "s"),
      "x" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "p1"), "x"), "n2" -> FLookup(FLookup(This(), "p2"), "x"))),
      "y" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "p1"), "y"), "n2" -> FLookup(FLookup(This(), "p2"), "y"))),
      "z" -> Init("Add", Map("n1" -> FLookup(FLookup(This(), "p1"), "z"), "n2" -> FLookup(FLookup(This(), "p2"), "z"))))),
    ClassT("Point", Map("s" -> PLookup(ThisP(), "s"), "x" -> ClassT("Nat"), "y" -> ClassT("Nat"), "z" -> ClassT("Nat"))))
  "Add(s: 3DSpace, p1: Point(s: this.s, x: Nat, y: Nat, z: Nat), p2: Point(s: this.s, x: Nat, y: Nat, z: Nat)" should "check" in {
    tAdd3D.check(List(tAdd, tAdd3D, tZero, t3DSpace, tSpace, tNat, t3DPoint))
  }

  def tAddSpace = Class("Add", Map("s" -> ClassT("Space"),
    "p1" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s"))),
    "p2" -> ClassT("Point", Map("s" -> PLookup(ThisP(), "s")))))
  "Add(s: Space, p1: Point(s: this.s), p2: Point(s: this.s)" should "check" in {
    tAddSpace.check(List(tAdd, tNat, tSpace, tAddSpace))
  }

  def tNewAddSpace = Init("Add", Map("s" -> Init("Space"),
    "p1" -> Init("Point", Map("s" -> Init("3DSpace"), "x" -> Init("Zero"), "y" -> Init("Zero"), "z" -> Init("Zero"))),
    "p2" -> Init("Point", Map("s" -> Init("2DSpace"), "x" -> Init("Zero"), "y" -> Init("Zero")))))

  "new Add(s = new Space, p1 = new Point(s = new 3DSpace, x = 0, y = 0, z = 0), p2 = new Point(s = new 2DSpace, x = 0, y = 0)" should "not typecheck" in {
    intercept[TypeException] {
      val classes = List(tAdd, tAddSpace, tNat, tZero, tSpace, t3DSpace, t2DSpace, tPoint, t2DPoint, t3DPoint)
      tNewAddSpace.typecheck(tNewAddSpace.getCtx(classes), classes)
    }
  }

  def tNewAddSpace2D = Init("Add", Map("s" -> Init("2DSpace"),
    "p1" -> Init("Point", Map("s" -> Init("2DSpace"), "x" -> Init("Zero"), "y" -> Init("Zero"))),
    "p2" -> Init("Point", Map("s" -> Init("2DSpace"), "x" -> Init("Zero"), "y" -> Init("Zero")))))

  "new Add(s = new 2DSpace, p1 = new Point(s = new 2DSpace, x = 0, y = 0), p2 = new Point(s = new 2DSpace, x = 0, y = 0)" should "typecheck" in {
    val classes = List(tAdd, tAddSpace, tNat, tZero, tSpace, t3DSpace, t2DSpace, tPoint, t2DPoint, t3DPoint)
    tNewAddSpace2D.typecheck(tNewAddSpace2D.getCtx(classes), classes)
  }

  def tEasyDep = Class("SomeDep", Map("x" -> ClassT("Nat"), "y" -> PLookup(ThisP(), "x")))
  "SomeDep(x: Nat, y: this.x)" should "check" in {
    tEasyDep.check(List(tNat, tEasyDep))
  }

  def tNewEasyDep = Init("SomeDep", Map("x" -> Init("Zero"), "y" -> Init("Zero")))
  "new SomeDep(x = new Zero(), y = new Zero())" should "typecheck" in {
    val classes = List(tNat, tEasyDep, tZero)
    tNewEasyDep.typecheck(tNewEasyDep.getCtx(classes), classes)
  }

  def tNewEasyDep2 = Init("SomeDep", Map("x" -> Init("Zero"), "y" -> Init("Succ", Map("pred" -> Init("Zero")))))
  "new SomeDep(x = new Zero(), y = new Succ(pred = new Zero()))" should "not typecheck" in {
    val classes = List(tNat, tEasyDep, tZero, tSucc)
    intercept[TypeException] {
      tNewEasyDep2.typecheck(tNewEasyDep2.getCtx(classes), classes)
    }
  }

  def tNestedDep = Class("SomeDep2", Map("x" -> ClassT("Nat"), "y" -> ClassT("SomeDep", Map("x" -> PLookup(ThisP(), "x"), "y" -> PLookup(ThisP(), "x")))))
  "SomeDep2(x: Nat, y: SomeDep(x = this.x, y = this.y), z = this.x)" should "check" in {
    tEasyDep.check(List(tNat, tEasyDep, tNestedDep))
  }

  def tNewNestedDep = Init("SomeDep2", Map("x" -> Init("Succ", Map("pred" -> Init("Zero"))), "y" -> tNewEasyDep))
  "new SomeDep2(x = new One(), y = new SomeDep(x = new Zero(), y = new Zero()))" should "not typecheck" in {
    val classes = List(tNat, tEasyDep, tNestedDep, tZero)
    intercept[TypeException] {
      tNewNestedDep.typecheck(tNewNestedDep.getCtx(classes), classes)
    }
  }

  def tNewNestedDep2 = Init("SomeDep2", Map("x" -> Init("Zero"), "y" -> Init("SomeDep", Map("x" -> FLookup(This(), "x"), "y" -> FLookup(This(), "x")))))
  "new SomeDep2(x = new Zero(), y = new SomeDep(x = this.x, y = this.x))" should "typecheck" in {
    val classes = List(tNat, tEasyDep, tNestedDep, tZero)
    tNewNestedDep2.typecheck(tNewNestedDep2.getCtx(classes), classes)
  }
  
  def tNewNestedDep3 = Init("SomeDep2", Map("x" -> Init("Zero"), "y" -> Init("SomeDep", Map("x" -> Init("Zero"), "y" -> Init("Zero")))))
  "new SomeDep2(x = new Zero(), y = new SomeDep(x = new Zero(), y = new Zero()))" should "typecheck" in {
    val classes = List(tNat, tEasyDep, tNestedDep, tZero)
    tNewNestedDep3.typecheck(tNewNestedDep3.getCtx(classes), classes)
  }
  
  def tATest = Class("A", Map("x" -> ClassT("Nat"), "y" -> PLookup(ThisP(), "x")))
  "A(x: Nat, y: this.x)" should "check" in {
    tATest.check(List(tATest, tNat))
  }
  
  def tBTest = Class("B", Map("x" -> ClassT("A"), "y" -> PLookup(PLookup(ThisP(), "x"), "y")), List(), Init("A", Map("x" -> FLookup(This(), "y"), "y" -> FLookup(This(), "y"))))
  "B(x: A, y: this.x.y) { new A(y, y) }" should "check" in {
    tBTest.check(List(tATest, tNat, tBTest))
  }
  
}